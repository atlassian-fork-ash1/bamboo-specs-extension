package com.atlassian.bamboo.specs.extension

import com.atlassian.bamboo.specs.api.builders.Variable
import com.atlassian.bamboo.specs.api.builders.notification.Notification
import com.atlassian.bamboo.specs.api.builders.notification.NotificationType
import com.atlassian.bamboo.specs.api.builders.plan.Plan
import com.atlassian.bamboo.specs.api.builders.plan.Stage
import com.atlassian.bamboo.specs.api.builders.trigger.AnyTrigger
import com.atlassian.bamboo.specs.api.builders.trigger.Trigger


fun Plan.stage(name: String, description: String? = null, finalStage: Boolean? = null, manual: Boolean? = null, init: SpecsDsl<Stage> = {}) {
    val stage = Stage(name)
    if (description != null)
        stage.description(description)
    if (finalStage != null)
        stage.finalStage(finalStage)
    if (manual != null)
        stage.manual(manual)
    stage.init()
    this.stages(stage)
}


fun <T : Trigger<in T, *>> Plan.trigger(triggerConstructor: () -> T, description: String? = null, enabled: Boolean? = null, init: SpecsDsl<T> = {}) {
    val trigger = triggerInitialisation(triggerConstructor, description, enabled, init)
    this.triggers(trigger)
}

fun Plan.trigger(triggerType: String, description: String? = null, enabled: Boolean? = null, init: SpecsDsl<AnyTrigger> = {}) {
    val trigger = triggerInitialisation(triggerType, description, enabled, init)
    this.triggers(trigger)
}

fun Plan.variable(name: String, value: String, init: SpecsDsl<Variable> = {}) {
    val variable = Variable(name, value)
    variable.init()
    this.variables(variable)
}

fun <T : NotificationType<in T, *>> Plan.notification(notificationTypeConstructor: () -> T, init: SpecsDsl<Notification> = {}) {
    val notification = notificationInitialisation(notificationTypeConstructor, init)
    this.notifications(notification)
}

fun Plan.notification(notificationTypeType: String, init: SpecsDsl<Notification> = {}) {
    val notification = notificationInitialisation(notificationTypeType, init)
    this.notifications(notification)
}
