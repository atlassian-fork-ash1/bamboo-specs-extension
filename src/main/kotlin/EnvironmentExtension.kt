package com.atlassian.bamboo.specs.extension

import com.atlassian.bamboo.specs.api.builders.AtlassianModule
import com.atlassian.bamboo.specs.api.builders.deployment.Environment
import com.atlassian.bamboo.specs.api.builders.deployment.configuration.AnyPluginConfiguration
import com.atlassian.bamboo.specs.api.builders.deployment.configuration.EnvironmentPluginConfiguration
import com.atlassian.bamboo.specs.api.builders.notification.Notification
import com.atlassian.bamboo.specs.api.builders.notification.NotificationType
import com.atlassian.bamboo.specs.api.builders.requirement.Requirement
import com.atlassian.bamboo.specs.api.builders.task.AnyTask
import com.atlassian.bamboo.specs.api.builders.task.Task
import com.atlassian.bamboo.specs.api.builders.trigger.AnyTrigger
import com.atlassian.bamboo.specs.api.builders.trigger.Trigger

fun Environment.requirement(key: String, matchType: Requirement.MatchType? = null, value: String? = null, init: SpecsDsl<Requirement> = {}) {
    val requirement = Requirement(key)
    if (matchType != null)
        requirement.matchType(matchType)
    if (value != null)
        requirement.matchValue(value)
    requirement.init()
    this.requirements(requirement)
}

fun <T : Task<in T, *>> Environment.task(constructor: () -> T, description: String? = null, enabled: Boolean? = null, init: SpecsDsl<T> = {}) {
    val task = taskInitialisation(constructor, description, enabled, init)
    this.tasks(task)
}

fun Environment.task(taskType: String, description: String? = null, enabled: Boolean? = null, init: SpecsDsl<AnyTask> = {}) {
    val task = taskInitialisation(taskType, description, enabled, init)
    this.tasks(task)
}

fun <T : Task<in T, *>> Environment.finalTask(constructor: () -> T, description: String? = null, enabled: Boolean? = null, init: SpecsDsl<T> = {}) {
    val finalTask = taskInitialisation(constructor, description, enabled, init)
    this.finalTasks(finalTask)
}

fun Environment.finalTask(taskType: String, description: String? = null, enabled: Boolean? = null, init: SpecsDsl<AnyTask> = {}) {
    val finalTask = taskInitialisation(taskType, description, enabled, init)
    this.finalTasks(finalTask)
}

fun <T : Trigger<in T, *>> Environment.trigger(construct: () -> T, description: String? = null, enabled: Boolean? = null, init: SpecsDsl<T> = {}) {
    val trigger = triggerInitialisation(construct, description, enabled, init)
    this.triggers(trigger)
}

fun Environment.trigger(triggerType: String, description: String? = null, enabled: Boolean? = null, init: SpecsDsl<AnyTrigger> = {}) {
    val trigger = triggerInitialisation(triggerType, description, enabled, init)
    this.triggers(trigger)
}

fun <T : NotificationType<in T, *>> Environment.notification(notificationTypeConstructor: () -> T, init: SpecsDsl<Notification> = {}) {
    val notification = notificationInitialisation(notificationTypeConstructor, init)
    this.notifications(notification)
}

fun Environment.notification(notificationTypeType: String, init: SpecsDsl<Notification> = {}) {
    val notification = notificationInitialisation(notificationTypeType, init)
    this.notifications(notification)
}

fun <T : EnvironmentPluginConfiguration<*>> Environment.pluginConfiguration(pluginConfigurationConstructor: () -> T, init: SpecsDsl<T> = {}) {
    val pluginConfiguration = pluginConfigurationConstructor()
    pluginConfiguration.init()
    this.pluginConfigurations(pluginConfiguration)
}

fun Environment.pluginConfiguration(pluginConfigurationType: String, init: SpecsDsl<AnyPluginConfiguration> = {}) =
        pluginConfiguration({ AnyPluginConfiguration(AtlassianModule(pluginConfigurationType)) }, init)
