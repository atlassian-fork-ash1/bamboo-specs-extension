package com.atlassian.bamboo.specs.extension

import com.atlassian.bamboo.specs.api.builders.RootEntityPropertiesBuilder
import com.atlassian.bamboo.specs.api.builders.deployment.Deployment
import com.atlassian.bamboo.specs.api.builders.deployment.Environment
import com.atlassian.bamboo.specs.api.builders.permission.DeploymentPermissions
import com.atlassian.bamboo.specs.api.builders.permission.EnvironmentPermissions
import com.atlassian.bamboo.specs.api.builders.permission.Permissions
import com.atlassian.bamboo.specs.api.builders.permission.PlanPermissions
import com.atlassian.bamboo.specs.api.builders.plan.Plan
import com.atlassian.bamboo.specs.api.builders.project.Project
import com.atlassian.bamboo.specs.util.BambooServer
import com.atlassian.bamboo.specs.util.BambooSpecSerializer
import java.io.Writer

object Specs {
    private val entities = mutableSetOf<RootEntityPropertiesBuilder<*>>()

    operator fun invoke(init: Specs.() -> Unit) = this.apply(init)

    fun project(key: String, name: String, description: String? = null, init: SpecsDsl<Project> = {}): Project {
        val project = Project()
        project.key(key)
        project.name(name)
        if (description != null)
            project.description(description)
        project.init()
        return project
    }

    @JvmName("projectPlan")
    fun plan(project: Project, key: String, name: String, description: String? = null, init: SpecsDsl<Plan> = {}): Plan {
        val plan = Plan(project, name, key)
        this.entities.add(plan)
        if (description != null)
            plan.description(description)
        plan.init()
        return plan
    }

    fun Project.plan(key: String, name: String, description: String? = null, init: SpecsDsl<Plan> = {}): Plan = plan(this, key, name, description, init)

    fun planPermissions(plan: Plan, setDefaults: Boolean = true, init: SpecsDsl<Permissions> = {}): PlanPermissions {
        val planPermissions = PlanPermissions(plan.identifier)
        this.entities.add(planPermissions)
        if (setDefaults)
            planPermissions.addDefaultPermissions()
        planPermissions.permissions.init()
        return planPermissions
    }

    fun Plan.permissions(setDefaults: Boolean = true, init: SpecsDsl<Permissions> = {}): PlanPermissions = planPermissions(this, setDefaults, init)

    @JvmName("planDeployment")
    fun deployment(plan: Plan, name: String, description: String? = null, init: SpecsDsl<Deployment> = {}): Deployment {
        val deployment = Deployment(plan.identifier, name)
        this.entities.add(deployment)
        if (description != null)
            deployment.description(description)
        deployment.init()
        return deployment
    }

    @JvmName("deploymentPermissions")
    fun deploymentPermissions(deployment: Deployment, init: SpecsDsl<Permissions> = {}): DeploymentPermissions {
        val deploymentPermissions = DeploymentPermissions(deployment.name)

        val permissions = Permissions()
        permissions.init()
        deploymentPermissions.permissions(permissions)

        entities.add(deploymentPermissions)
        return deploymentPermissions
    }

    fun Deployment.permissions(init: SpecsDsl<Permissions> = {}) = deploymentPermissions(this, init)

    fun environmentPermissions(deploymentProjectName: String, environmentName: String, init: SpecsDsl<Permissions> = {}): EnvironmentPermissions {
        val environmentPermissions = EnvironmentPermissions(deploymentProjectName, environmentName)

        val permissions = Permissions()
        permissions.init()
        environmentPermissions.permissions(permissions)

        entities.add(environmentPermissions)
        return environmentPermissions
    }

    fun Environment.permissions(deploymentProjectName: String, environmentName: String, init: SpecsDsl<Permissions> = {}) = environmentPermissions(deploymentProjectName, environmentName, init)

    fun Plan.deployment(name: String, description: String? = null, init: SpecsDsl<Deployment> = {}): Deployment = deployment(this, name, description, init)


    fun publish(bambooServer: BambooServer) = entities.forEach { bambooServer.publish(it) }

    fun printYaml(writer: Writer) = entities.forEach { BambooSpecSerializer.dump(it, writer) }

    fun toYaml(): String = entities.joinToString("\n") { BambooSpecSerializer.dump(it) }
}
