package com.atlassian.bamboo.specs.extension

import com.atlassian.bamboo.specs.api.builders.deployment.Deployment
import com.atlassian.bamboo.specs.api.builders.deployment.Environment

fun Deployment.environment(name: String, description: String? = null, init: SpecsDsl<Environment> = {}) {
    val environment = Environment(name)
    if (description != null)
        environment.description(description)
    environment.init()
    this.environments(environment)
}
