package com.atlassian.bamboo.specs.extension

@DslMarker
@Target(AnnotationTarget.CLASS, AnnotationTarget.TYPE)
annotation class SpecsMarker
