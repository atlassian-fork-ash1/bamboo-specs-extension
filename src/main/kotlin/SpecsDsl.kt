package com.atlassian.bamboo.specs.extension

import com.atlassian.bamboo.specs.util.BambooServer
import com.atlassian.bamboo.specs.util.BambooSpecSerializer
import java.io.Writer

typealias SpecsDsl<T> = (@SpecsMarker T).() -> Unit

fun BambooServer.publish(specs: Specs) = specs.publish(this)
fun BambooSpecSerializer.dump(specs: Specs, writer: Writer) = specs.printYaml(writer)
fun BambooSpecSerializer.dump(specs: Specs) = specs.toYaml()
