import com.atlassian.bamboo.specs.api.BambooSpec
import com.atlassian.bamboo.specs.api.builders.deployment.Deployment
import com.atlassian.bamboo.specs.api.builders.deployment.ReleaseNaming
import com.atlassian.bamboo.specs.api.builders.permission.PermissionType
import com.atlassian.bamboo.specs.api.builders.plan.Plan
import com.atlassian.bamboo.specs.builders.notification.DeploymentStartedAndFinishedNotification
import com.atlassian.bamboo.specs.builders.notification.HipChatRecipient
import com.atlassian.bamboo.specs.builders.task.ArtifactDownloaderTask
import com.atlassian.bamboo.specs.builders.task.CleanWorkingDirectoryTask
import com.atlassian.bamboo.specs.builders.task.ScriptTask
import com.atlassian.bamboo.specs.builders.task.VcsCheckoutTask
import com.atlassian.bamboo.specs.builders.trigger.AfterSuccessfulBuildPlanTrigger
import com.atlassian.bamboo.specs.builders.trigger.ScheduledDeploymentTrigger
import com.atlassian.bamboo.specs.extension.Specs
import com.atlassian.bamboo.specs.extension.Specs.permissions
import com.atlassian.bamboo.specs.extension.allArtifacts
import com.atlassian.bamboo.specs.extension.artifact
import com.atlassian.bamboo.specs.extension.checkout
import com.atlassian.bamboo.specs.extension.environment
import com.atlassian.bamboo.specs.extension.finalTask
import com.atlassian.bamboo.specs.extension.notification
import com.atlassian.bamboo.specs.extension.recipient
import com.atlassian.bamboo.specs.extension.task
import com.atlassian.bamboo.specs.extension.trigger

@BambooSpec
class ProjectSpec {
    fun deployment(plan: Plan): Deployment {
        return Specs.deployment(plan, "Puppet CD Deploy", "Deploy Puppet CD results to puppet master ###Managed by Plan Templates###") {
            releaseNaming(ReleaseNaming("release-\${bamboo.buildNumber}"))

            permissions {
                userPermissions("buildeng-build-doctor", PermissionType.VIEW)
                loggedInUserPermissions(PermissionType.VIEW, PermissionType.BUILD)
                anonymousUserPermissionView()
            }

            environment("Puppet Staging", "Transfer to puppetmaster and change symlinks for staging") {
                permissions("Puppet CD Deploy", "Puppet Staging") {
                    userPermissions("buildeng-build-doctor", PermissionType.VIEW)
                    userPermissions("pleschev", PermissionType.BUILD, PermissionType.VIEW)
                    loggedInUserPermissions(PermissionType.VIEW, PermissionType.BUILD)
                    anonymousUserPermissionView()
                }

                task(::ArtifactDownloaderTask, "artifactDownload") {
                    artifact("Puppet deployment", ".")
                }
                task(::ScriptTask, "reuse artifact from previous stage") {
                    inlineBody("\n#!/bin/sh\ntar -xjf PUPPET_DEPLOY-\${bamboo.buildNumber}.tar.bz2\n            ")
                }
                task(::ScriptTask, "transfer to buildeng puppet master") {
                    inlineBody("\n#!/bin/sh\nset -e\nfor host in puppet.staging.aws.buildeng.atlassian.com puppet.production.aws.buildeng.atlassian.com; do\n./utils/ci/transfer_to_puppetmaster.sh   -u \"buildeng-puppet-ci\"   -k \"/opt/bamboo-agent/.ssh/id_dsa\"   -p \$host   -a \"PUPPET_DEPLOY-\${bamboo.buildNumber}.tar.bz2\"   -d \"/data/etc/puppetlabs/deployments\"\ndone\n        ")
                }
                task(::ScriptTask, "change symlinks for staging") {
                    inlineBody("\n#!/bin/sh\nset -e\nfor host in puppet.staging.aws.buildeng.atlassian.com puppet.production.aws.buildeng.atlassian.com; do\n./utils/ci/update_puppetmaster_symlinks.sh   -u \"buildeng-puppet-ci\"   -k \"/opt/bamboo-agent/.ssh/id_dsa\"   -p \$host   -c \"PUPPET_DEPLOY-\${bamboo.buildNumber}\"   -t \"staging\"   -e \"/data/etc/puppetlabs/deployment-environments\"   -d \"/data/etc/puppetlabs/deployments\"\ndone\n        ")
                }
                finalTask("com.atlassian.buildeng.bamboo-isolated-docker-plugin:dockertask") {
                    configuration(mapOf(
                            "dockerImageSize" to "SMALL",
                            "dockerImage" to "docker.atl-paas.net/buildeng/agent-buildpipeline",
                            "userDescription" to "",
                            "taskDisabled" to "false"))
                }
                trigger(::AfterSuccessfulBuildPlanTrigger, "afterGREEN")
            }
            environment("Puppet Production", "Puppet Production environment on Puppet Master") {
                task(::VcsCheckoutTask, "checkout Puppet repository") {
                    checkout("Buildeng Puppet Tree")
                }
                task(::ScriptTask, "change production symlinks") {
                    inlineBody("\n#!/bin/sh\nset -e\nfor host in puppet.staging.aws.buildeng.atlassian.com puppet.production.aws.buildeng.atlassian.com; do\n./utils/ci/update_puppetmaster_symlinks.sh   -u \"buildeng-puppet-ci\"   -k \"/opt/bamboo-agent/.ssh/id_dsa\"   -p \$host   -c \"PUPPET_DEPLOY-\${bamboo.buildNumber}\"   -t \"production\"   -e \"/data/etc/puppetlabs/deployment-environments\"   -d \"/data/etc/puppetlabs/deployments\"\ndone\n        ")
                }
                task(::ScriptTask, "Remove stale deployments") {
                    inlineBody("\n#!/bin/bash\n\nset -e\n\nPUPPET_DEPLOY_USER=\"buildeng-puppet-ci\"\nPUPPET_DEPLOY_KEY=\"/opt/bamboo-agent/.ssh/id_dsa\"\nPUPPET_DEPLOY_PUPPETMASTER=\"puppet.staging.aws.buildeng.atlassian.com puppet.production.aws.buildeng.atlassian.com\"\nPUPPET_DEPLOY_ENV=\"production\"\nPUPPET_DEPLOY_ENV_DIR=\"/data/etc/puppetlabs/deployment-environments\"\n\nfor host in \${PUPPET_DEPLOY_PUPPETMASTER} ; do\nssh -p 2222 -o StrictHostKeyChecking=no -i \"\${PUPPET_DEPLOY_KEY}\" \"\${PUPPET_DEPLOY_USER}@\${host}\"   \"cd \${PUPPET_DEPLOY_ENV_DIR}/\${PUPPET_DEPLOY_ENV}/current &&\n   utils/ci/remove_stale_puppet_deployments.sh      -e /data/etc/puppetlabs/deployment-environments      -d /data/etc/puppetlabs/deployments\"\ndone\n        ")
                }
                finalTask("com.atlassian.buildeng.bamboo-isolated-docker-plugin:dockertask") {
                    configuration(mapOf(
                            "dockerImageSize" to "SMALL",
                            "dockerImage" to "docker.atl-paas.net/buildeng/agent-buildpipeline",
                            "userDescription" to "",
                            "taskDisabled" to "false"))
                }
                trigger(::ScheduledDeploymentTrigger, "Daily Puppet Deployment") {
                    cronExpression("0 0 20 ? * *")
                }
                notification(::DeploymentStartedAndFinishedNotification) {
                    recipient(::HipChatRecipient) {
                        apiToken("/* PUT YOUR SENSITIVE INFORMATION HERE*/")
                        room("Build Engineering - Private")
                    }
                    recipient("com.atlassian.bamboo.plugins.bamboo-stride-plugin:recipient.stride") {
                        recipientString("Build Engineering - Private")
                    }
                }
            }
            environment("Deploy AMIs to staging-bamboo", "Update staging-bamboo to use generated AMIs") {
                task(::CleanWorkingDirectoryTask, "clean up")
                task(::ArtifactDownloaderTask, "Downloading Puppet deployment") {
                    allArtifacts(".")
                }
                task(::ScriptTask, "reuse artifact from previous stage") {
                    inlineBody("\n#!/bin/sh\ntar -xjf PUPPET_DEPLOY-\${bamboo.buildNumber}.tar.bz2\n            ")
                }
                task(::ScriptTask, "Install runtime dependencies") {
                    inlineBody("\n#!/bin/sh\nset -e\nvirtualenv venv\nsource venv/bin/activate\npip install boto requests\n    ")
                }
            }
        }
    }
}
