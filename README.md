# Kotlin extension for bamboo specs
A [Kotlin](https://kotlinlang.org/) library that creates an elegant, precise dsl for [Bamboo Java Specs](https://confluence.atlassian.com/bamboo/bamboo-java-specs-941616821.html).

## Sample of bamboo specs

```
@BambooSpec
object PlanSpecs {
    @JvmStatic
    fun main(argv: Array<String>) {
        Specs {
            project("MAVENPLUGINS", "Maven plugins", "Project to all maven plugins.") {
                plan("SOURCEDISTRIBUTIONPLUGIN", "Source distribution plugin", "Build and verify") {
                    linkedRepositories("my linked repo")
                    stage("checkout, build", "build") {
                        data class JobDetail(val key: String, val name: String, val mavenVersion: String)
                        val jobDetails = listOf(
                                JobDetail("BUILDMVN32", "checkout and build - maven 3.2", "3.2.5"),
                                JobDetail("BUILDMVN30", "checkout and build - maven 3.0", "3.0.5"))
                        for (jobDetail in jobDetails) {
                            job(jobDetail.key, jobDetail.name) {
                                task(::VcsCheckoutTask, "Checkout source") {
                                    checkoutDefault()
                                }
                                task(::ScriptTask, "build the plugin") {
                                    inlineBody("mvn --mvn-version ${jobDetail.mavenVersion} clean verify -B")
                                    environmentVariables("JAVA_HOME" to "\${bamboo.capability.system.jdk.JDK 1.8}",
                                            "PATH" to "\${bamboo.capability.system.jdk.JDK 1.8}/bin:\$PATH")
                                }
                                artifact("Integration tests", "target/it", "**/*.*")
                                artifact("Unit tests", "target/surefire-reports", "**/*.*")
                            }
                        }
                    }
                    stage("Release", "Release a new version", manual = true) {
                        job("RELEASE", "Release a new version", "Release a new version of the plugin") {
                            task(::VcsCheckoutTask, "Checkout source") {
                                checkoutDefault()
                            }
                            task(::ScriptTask, "maven release") {
                                inlineBody("mvn release:prepare release:perform")
                                environmentVariables("JAVA_HOME" to "\${bamboo.capability.system.jdk.JDK 1.8}",
                                        "PATH" to "\${bamboo.capability.system.jdk.JDK 1.8}/bin:\$PATH")
                            }
                        }
                    }
                    trigger(::BitbucketServerTrigger, "Bitbucket Server Trigger")
                    planBranchManagement(BranchManagementType::Vcs, INHERIT, INHERITED) {
                        cleanup(true, true, 1, 7)
                    }

                    notification(::JobFailedNotification) { recipient(::CommittersRecipient) }
                }
            }
        }
        BambooServer("https://bamboo.internal.abc.com").publish(Specs)
    }
}

``` 
After finishing your specs.kt, you an run normal `mvn -Ppublish-specs` or use [Repository Stored Specs]() 
## How to use 
Add it as a dependency in your specs' pom.xml
```
    <dependency>
      <groupId>com.atlassian.bamboo.specs.extension</groupId>
      <artifactId>bamboo-specs-extension</artifactId>
      <version>1.0.1</version>
    </dependency>
```
Also add following section in `<build>` segment of pom.xml
```
    <sourceDirectory>${project.basedir}/src/main/kotlin</sourceDirectory>
    <plugins>
      <plugin>
        <artifactId>kotlin-maven-plugin</artifactId>
        <groupId>org.jetbrains.kotlin</groupId>
        <version>1.2.41</version>
        <executions>
          <execution>
            <id>compile</id>
            <goals> <goal>compile</goal> </goals>
          </execution>
          <execution>
            <id>test-compile</id>
            <goals> <goal>test-compile</goal> </goals>
          </execution>
        </executions>
      </plugin>
    </plugins>
```
After finished editing your specs.kt, you can either run `mvn -Ppublish-specs` or use [Repository Stored Specs](https://confluence.atlassian.com/bamboo/enabling-repository-stored-bamboo-specs-938641941.html)
## Note: 
If you use [Repository Stored Specs](https://confluence.atlassian.com/bamboo/enabling-repository-stored-bamboo-specs-938641941.html), bamboo by default will sanitize you pom.xml by removing all maven plugins except maven-compile-plugin. So your kotlin code won't be compiled.
To use this dsl, you need to disable pom sanitizing by adding `-Dbamboo.repository.stored.specs.pom.sanitization.enabled=false` to bamboo startup script